﻿
namespace Authentication.Twitter
{
    /// <summary>
    /// Default values used by the Twitter authentication middleware
    /// </summary>
    public static class TwitterAuthenticationDefaults
    {
        /// <summary>
        /// System name of the external authentication method
        /// </summary>
        public const string ProviderSystemName = "ExternalAuth.Twitter";

        public const string ConfigurationUrl = "../TwitterAuthenticationSettings/Configure";
    }
}
