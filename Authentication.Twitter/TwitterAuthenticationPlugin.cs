using Grand.Business.Core.Extensions;
using Grand.Business.Core.Interfaces.Common.Configuration;
using Grand.Business.Core.Interfaces.Common.Localization;
using Grand.Infrastructure.Plugins;

namespace Authentication.Twitter
{
    /// <summary>
    /// Represents method for the authentication with Twitter account
    /// </summary>
    public class TwitterAuthenticationPlugin : BasePlugin, IPlugin
    {
        #region Fields

        private readonly ISettingService _settingService;
        private readonly ITranslationService _translationService;
        private readonly ILanguageService _languageService;

        #endregion

        #region Ctor

        public TwitterAuthenticationPlugin(ISettingService settingService,
            ITranslationService translationService,
            ILanguageService languageService)
        {
            _settingService = settingService;
            _translationService = translationService;
            _languageService = languageService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets a configuration page URL
        /// </summary>
        public override string ConfigurationUrl()
        {
            return TwitterAuthenticationDefaults.ConfigurationUrl;
        }



        /// <summary>
        /// Install the plugin
        /// </summary>
        public override async Task Install()
        {
            //settings
            await _settingService.SaveSetting(new TwitterExternalAuthSettings());

            //locales
            await this.AddOrUpdatePluginTranslateResource(_translationService, _languageService, "Authentication.Twitter.Login", "Login using Twitter account");
            await this.AddOrUpdatePluginTranslateResource(_translationService, _languageService, "Authentication.Twitter.Failed", "Twitter - Login error");
            await this.AddOrUpdatePluginTranslateResource(_translationService, _languageService, "Authentication.Twitter.Failed.ErrorCode", "Error code");
            await this.AddOrUpdatePluginTranslateResource(_translationService, _languageService, "Authentication.Twitter.Failed.ErrorMessage", "Error message");
            await this.AddOrUpdatePluginTranslateResource(_translationService, _languageService, "Authentication.Twitter.DisplayOrder", "Display order");
            
            await base.Install();
        }

        /// <summary>
        /// Uninstall the plugin
        /// </summary>
        public override async Task Uninstall()
        {
            //settings
            await _settingService.DeleteSetting<TwitterExternalAuthSettings>();

            //locales
            await this.DeletePluginTranslationResource(_translationService, _languageService, "Authentication.Twitter.Login");

            await base.Uninstall();
        }

        #endregion
    }
}
