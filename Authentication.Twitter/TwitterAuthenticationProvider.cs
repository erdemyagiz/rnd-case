﻿using Grand.Business.Core.Interfaces.Authentication;

namespace Authentication.Twitter
{
    public class TwitterAuthenticationProvider : IExternalAuthenticationProvider
    {
        private readonly TwitterExternalAuthSettings _TwitterExternalAuthSettings;

        public TwitterAuthenticationProvider(TwitterExternalAuthSettings TwitterExternalAuthSettings)
        {
            _TwitterExternalAuthSettings = TwitterExternalAuthSettings;
        }
        public string SystemName => TwitterAuthenticationDefaults.ProviderSystemName;

        public string FriendlyName => "Twitter authentication";

        public int Priority => _TwitterExternalAuthSettings.DisplayOrder;

        public string ConfigurationUrl => TwitterAuthenticationDefaults.ConfigurationUrl;

        public IList<string> LimitedToStores => new List<string>();

        public IList<string> LimitedToGroups => new List<string>();

        /// <summary>
        /// Gets a view component for displaying plugin in public store
        /// </summary>
        public async Task<string> GetPublicViewComponentName()
        {
            return await Task.FromResult("TwitterAuthentication");
        }
    }
}
