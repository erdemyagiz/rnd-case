﻿using Authentication.Twitter;
using Grand.Infrastructure;
using Grand.Infrastructure.Plugins;

[assembly: PluginInfo(
    FriendlyName = "Twitter authentication",
    Group = "Authentication methods",
    SystemName = TwitterAuthenticationDefaults.ProviderSystemName,
    SupportedVersion = GrandVersion.SupportedPluginVersion,
    Author = "grandnode team",
    Version = "2.1.1"
)]