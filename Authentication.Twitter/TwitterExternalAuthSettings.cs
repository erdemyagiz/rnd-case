using Grand.Domain.Configuration;

namespace Authentication.Twitter
{
    /// <summary>
    /// Represents settings of the Twitter authentication method
    /// </summary>
    public class TwitterExternalAuthSettings : ISettings
    {        
        /// <summary>
        /// Gets or sets display order
        /// </summary>
        public int DisplayOrder { get; set; }
    }
}
