﻿using Grand.Infrastructure.Endpoints;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;

namespace Authentication.Twitter
{
    public class EndpointProvider : IEndpointProvider
    {
        public void RegisterEndpoint(IEndpointRouteBuilder endpointRouteBuilder)
        {
            endpointRouteBuilder.MapControllerRoute("Plugin.ExternalAuth.Twitter.SignInTwitter",
                 "twitter-signin-failed",
                 new { controller = "TwitterAuthentication", action = "TwitterSignInFailed" }
            );
        }
        public int Priority => 10;
    }
}
