﻿using Grand.Infrastructure.Models;

namespace Authentication.Twitter.Models
{
    public class FailedModel : BaseModel
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }
}
