﻿using Grand.Infrastructure.ModelBinding;
using Grand.Infrastructure.Models;

namespace Authentication.Twitter.Models
{
    public class ConfigurationModel : BaseModel
    {
        [GrandResourceDisplayName("Authentication.Twitter.ConsumerKey")]
        public string ConsumerKey { get; set; }

        [GrandResourceDisplayName("Authentication.Twitter.ConsumerSecret")]
        public string ConsumerSecret { get; set; }

        [GrandResourceDisplayName("Authentication.Twitter.DisplayOrder")]
        public int DisplayOrder { get; set; }
    }
}