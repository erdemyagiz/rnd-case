﻿using Grand.Business.Core.Interfaces.Authentication;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Twitter;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Net;

namespace Authentication.Twitter.Infrastructure
{
    /// <summary>
    /// Registration of Twitter authentication service (plugin)
    /// </summary>
    public class TwitterAuthenticationBuilder : IAuthenticationBuilder
    {
        /// <summary>
        /// Configure
        /// </summary>
        /// <param name="builder">Authentication builder</param>
        /// <param name="configuration">Configuration</param>
        public void AddAuthentication(AuthenticationBuilder builder, IConfiguration configuration)
        {
            builder.AddTwitter(TwitterDefaults.AuthenticationScheme, options =>
            {
                var appId = configuration["TwitterSettings:ConsumerKey"];
                var appSecret = configuration["TwitterSettings:ConsumerSecret"];

                options.ConsumerKey = !string.IsNullOrWhiteSpace(appId) ? appId : "000";
                options.ConsumerSecret = !string.IsNullOrWhiteSpace(appSecret) ? appSecret : "000";
                options.RetrieveUserDetails = true;
                options.Events = new TwitterEvents() {
                    OnCreatingTicket = context =>
                    {
                        System.Diagnostics.Debug.WriteLine($"TwitterEvents.OnCreatingTicket: UserId = {context.UserId}");
                        System.Diagnostics.Debug.WriteLine($"TwitterEvents.OnCreatingTicket: AccessToken = {context.AccessToken}");
                        System.Diagnostics.Debug.WriteLine($"TwitterEvents.OnCreatingTicket: AccessTokenSecret = {context.AccessTokenSecret}");
                        System.Diagnostics.Debug.WriteLine($"TwitterEvents.OnCreatingTicket: User = {context.User}");
                        return Task.CompletedTask;
                    }
                };
               
            });
        }

        /// <summary>
        /// Gets order of this registrar implementation
        /// </summary>
        public int Priority => 501;
    }
}