﻿using Microsoft.AspNetCore.Mvc;

namespace Authentication.Twitter.Components
{
    [ViewComponent(Name = "TwitterAuthentication")]
    public class TwitterAuthenticationViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View();
        }
    }
}