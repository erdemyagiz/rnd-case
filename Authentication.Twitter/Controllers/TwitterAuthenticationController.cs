﻿using Authentication.Twitter.Models;
using Grand.Business.Core.Interfaces.Authentication;
using Grand.SharedKernel;
using Grand.Web.Common.Controllers;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Twitter;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Security.Claims;
using Grand.Business.Core.Utilities.Authentication;

namespace Authentication.Twitter.Controllers
{
    public class TwitterAuthenticationController : BasePluginController
    {
        #region Fields

        private readonly IExternalAuthenticationService _externalAuthenticationService;
        private readonly IConfiguration _configuration;

        #endregion

        #region Ctor

        public TwitterAuthenticationController(
            IExternalAuthenticationService externalAuthenticationService,
            IConfiguration configuration)
        {
            _externalAuthenticationService = externalAuthenticationService;
            _configuration = configuration;
        }

        #endregion

        #region Methods

        public IActionResult TwitterLogin(string returnUrl)
        {
            if (!_externalAuthenticationService.AuthenticationProviderIsAvailable(TwitterAuthenticationDefaults.ProviderSystemName))
                throw new GrandException("Twitter authentication module cannot be loaded");

            if (string.IsNullOrEmpty(_configuration["TwitterSettings:ConsumerKey"]) || string.IsNullOrEmpty(_configuration["TwitterSettings:ConsumerSecret"]))
                throw new GrandException("Twitter authentication module not configured");

            //configure login callback action
            var authenticationProperties = new AuthenticationProperties {
                RedirectUri = Url.Action("TwitterLoginCallback", "TwitterAuthentication", new { returnUrl })
            };

            return Challenge(authenticationProperties, TwitterDefaults.AuthenticationScheme);
        }

        public async Task<IActionResult> TwitterLoginCallback(string returnUrl)
        {
            //authenticate Twitter user
            var authenticateResult = await HttpContext.AuthenticateAsync(TwitterDefaults.AuthenticationScheme);
            if (!authenticateResult.Succeeded || !authenticateResult.Principal.Claims.Any())
                return RedirectToRoute("Login");

            //create external authentication parameters
            var authenticationParameters = new ExternalAuthParam {
                ProviderSystemName = TwitterAuthenticationDefaults.ProviderSystemName,
                AccessToken = await HttpContext.GetTokenAsync(TwitterDefaults.AuthenticationScheme, "access_token"),
                Email = authenticateResult.Principal.FindFirst(claim => claim.Type == ClaimTypes.Email)?.Value,
                Identifier = authenticateResult.Principal.FindFirst(claim => claim.Type == ClaimTypes.NameIdentifier)?.Value,
                Name = authenticateResult.Principal.FindFirst(claim => claim.Type == ClaimTypes.Name)?.Value,
                Claims = authenticateResult.Principal.Claims.ToList()
            };

            //authenticate Grand user
            return await _externalAuthenticationService.Authenticate(authenticationParameters, returnUrl);
        }
        public IActionResult TwitterSignInFailed(string error_code, string error_message, string state)
        {
            //handle exception and display message to user
            var model = new FailedModel() {
                ErrorCode = error_code,
                ErrorMessage = error_message
            };
            return View(model);
        }


        #endregion
    }
}